# Good Practices for Software Development

## Question 1 : What is your one major takeaway from each one of the 6 sections.

- **Gathering requirements** : Make notes, ask relevant questions to have better understanding of each aspect about the project while in the meeting.
- **Always over-communicate:** Inform relevant team members.
- **Stuck? Ask Quetions:**  Explain the problem clearly, mention the solutions you tried out to fix the problem
- **Get to know your teammates:** • Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
- **Be aware and mindful of other team members:** • Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.
- **Doing things with 100% involvement:** • Programming is essentially a result of your sustained and concentrated attention

## Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I take lots of time to figure out the problems and bugs in the code.
- I need to refer the technology documentations rather than relying on trial and error.