# Energy Management

## 1. Manage Your Energy Not Your Time

**Question 1: What are the activities you do that make you relax - Calm quadrant?**

There are multiple things I do to make myself calm and relaxed. These are as follows : 
* Listen to music. 
* Watch a movie. 
* Listen to a podcast. 
* Read a novel. 
* Talk to friends and relatives. 
* Take a nap. 
* Make a cup of tea for me. 

**Question 2: When do you find getting into the Stress quadrant?**

* When I am way behind the deadline. 
* When someone is mean to me. 
* When I call a friend He ingnores my call.
* When I join a zoom call and camera or mic doesn't work. 
* When there is a code review. 

**Question 3: How do you understand if you are in the Excitement quadrant?**

* When I successfully book a train ticket in tatkal quota. 
* Planning for a trip. 
* Talking to my friends about a movie. 
* When I finished a project. 
* Receiving a delivery and opening the package. 

## Sleep is your superpower

**Question 4: Paraphrase the Sleep is your Superpower video in detail.**

* Sleep is very very essential for our brain. After a quality sleep, our brain absorbs more information if We try to learn something. 
* 8 hours of good night's sleep is considered ideal for a human being. 
* Lack of deep-quality sleep can cause rapid aging. 
* Lack of sleep can cause immune deficiency. One can lose 75% of killer cells which tend to fight the germs and viruses inside our body. 
* Our DNA can get distorted if we don't sleep enough. That directly increases the genes related to terminal diseases. 
* Go to bed at the same time every day. Stay consistent with your bedtime. 

**Question 5: What are some ideas that you can implement to sleep better?**

* Stop staring at screens 2 hours prior to hit the bed. 
* Eat healthily. Do not consume fast foods regularly. 
* Do some lite exercise or stretching daily. 
* Listen to white noise if you're struggling to fall asleep. 

## Brain-Changing Benefits of Exercise

**Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

* Physical activity has long-lasting benefits for our bodies. It can keep our body away from diseases like Alzheimers and Demeschia. 
* Prefrontal Cortex is responsible for decision-making, personality, focus, and attention.
* There are two Temporal Lobes in the brain.
* The hippocampus is the brain part located inside the Temporal Lobes which is essential for long-term memory.   
* Exercise is the most crucial thing for the health and development of the brain. It has three immediate effects: 
  * It increases the level of neurotransmitters like Dopamine, Serotonin, and Adrenaline which lift up the mood and keep the body energetic the whole day. 
  * Exercise makes the Prefrontal Cortex and hippocampus bigger and strong and protects these two from neurodegenerative diseases. 
  * Exercise produces new brain cells and improve long term memory. 
* Exercises like cardiovascular and aerobics are well suited for the brains and hearts. 

**Question 7: What are some steps you can take to exercise more?**

* Goto bed and wake up early so that I get some spare time in the morning for lite exercise. 
* Do five-minute skipping in the morning. 
* Do some stretching at an interval of one or one and a half hours. 