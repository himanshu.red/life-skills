# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### **What is the Feynman Technique? Paraphrase the video in your own words.**

Feynman Technique is a way to learn any concept faster and to have a deep understanding of that concept. According to this technique, after learning a topic, you try to teach the concept to a person. The language should be simple so that the person can grasp everything easily. If you get stuck while teaching, It means that you haven't understood the concept well. You still have some doubts to clear. After clearing the doubt repeat the process until you successfully convey everything about the topic.

### **What are the different ways to implement this technique in your learning process?**

I can leverage this technique in two ways in my learning process: 

1.  After learning about a concept I will try to implement it right away. Let's say I just learnt about the CSS Flexbox. I will try to make a small project using that technology. 

2.  Another way is to discuss the concepts with my friends that I just freshly learned. 

## 2. Learning How to Learn TED talk by Barbara Oakley

### **Paraphrase the video in detail in your own words.**
* The brain has two modes: The focus mode and the diffused mode. 

* While learning, the brain goes back and forth between these two modes. 

* When You're stuck on a problem, You need to move your attention from that problem and activate the diffused mode for a small mental rest. 

* For most people, taking a break means they are procrastinating. They are running away from the problem. 

* There is a solution to this procrastination problem. It is called Pomodoro technique. 

* The Pomodoro technique says that one should work for 25 minutes deeply and take 5 minutes break. This way your attention span remains the same. 

* Slow thinkers and people with loose memory get to know more nuances about whatever They're learning. They just have to work a bit harder than people with good memory. They have a good grip on the learnings after they finish. 

* According to researchers and scientists these are the following way of most effective learning.  

  * Physical Exercise.

  * Giving test. 

  * Redoing homework multiple times. 

  * Active recall. 

  * Understanding and practicing the learnings. 

### **What are some of the steps that you can take to improve your learning process?**

1. Inculcating the Pomodoro technique into my workflow. 

2. Taking a small break if I get stuck on a problem instead of deliberately doing nonsensical things just to get it done anyhow. 

3. Eating healthy. 

4. Taking solid 8 hours of sleep. 

5. Getting self-disciplined. 

## 3. Learn Anything in 20 hours

### **Your key takeaways from the video? Paraphrase your understanding.**

* The statement is this "It takes 10,000 hrs **to become an expert** at something." People have rephrased it "It takes 10,000 hrs to **learn** something."

* Practically, It takes only 20 hrs of focused deliberate practice to learn something or be good enough at something. 

* Methods of doing the first 20 hrs to learn something new

  * **Deconstruct the skill**: Be ultra clear about what you want to achieve after learning that skill. Break down the skills into smaller and smaller pieces. 
  
  * **Learn enough to self-correct**: You should learn just enough to get you going, to self-correct if your learning mechanism is slowing down or you're making a mistake. 

  * **Remove practice barriers**: Do not get distracted while doing the work. Remove all the distractions from your eye-sight. 

  * **Practice at least 20 hours**: Going through the first couple of hrs is very frustrating as we don't know anything about that topic. But one should be determined enough to complete it. 
  
### **What are some of the steps that you can while approaching a new topic?**
  
  * To be clear about what I want to achieve after learning this new skill. 

  * Break the lessons into smaller chunks before starting to learn. 

  * Make the learning environment distraction-free. 
