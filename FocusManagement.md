# Focus Management

## What is deep work? 

### Question 1: What is deep work?

* When one focuses without distraction on a cognitively demanding tasks. 

## Optimal duration for Deep Work, Are Deadlines good for productivity?, Summary of Deep Work Book

### Question 2: Paraphrase all the ideas in the above videos and this one in detail.

- Optimal duration of deep work.
    - According to Cal Newport, the optimal duration of deep work is 1 hour.
    - But it depends on person to person. This deep focus duration for someone can vary from 25 minutes to one and half hour.
- Are Deadlines good for productivity?
    - Yes, The deadlines work like a clear motivation.
    - If one has to complete a deadline approaching nearby, He/she doesn’t argue with himself or herself about when he/she is gonna work to meet the deadline.
    - Without a deadline our brain tries to procrastinate as much as possible. Unless and until finishing that work is really necessary.
- Summary of Deep Work book
    - According to Cal Newport, The deep work is a professional activities performed in a state of distraction free concentration that pushes your cognitive abilities to their limit.
    - Harry Potter writer J. K. Rowling finished her last book using the deep work technique. At home she was not able to focus well because of the kids noise and shouts. So she decided to go to  a five star hotel in Scotland for a period of time to finish up the book.
    - The author himself uses this technique to stay productive while raising a family.
    - Bill Gates used the deep work technique to write the foundation code for Microsoft.
    - When we use deep work frequently to finish our daily tasks, our brain cells make new neural circuit. Once we make this technique a habit, those circuits are fired up so quickly and effortlessly.
    - In a 2012 study done  on two hundred and fifty adults by Hofmann and Baumeister, They were asked to only check the emails and social media in break. Fifty percent of people failed.
    - Three strategies to inculcate deep work in your life.
        - Schedule your distraction period.
        - Develop a rhythmic deep work ritual
        - Set a time to keep all your gadget away from you in the evening and adequate sleep to keep yourself more focused and concentrated. 
## Question 3: How can you implement the principles in your day to day life?
- I can schedule a distraction period for the day.
- I can use Pomodoro technique to implement the deep work technique.
- I can set a night time routine to get quality sleep.

## Dangers of social media

### Question 4: Your key takeaway from the video.

- The speaker is trying to convince the audience to quit social media.
- Social media is one of the biggest distraction for a person whose work is a mental work.
- Social media companies are hiring attention engineers who try to implement the Las Vegas casino principle to their social media platform in order to keep the people hooked for longer period of time to their platform.
- According to the author If one produces which is rare and valuable, He/she will be known for his/her work.
- Some well documented  and significant harms of using social media :
    - Social media causes distraction and distraction produces shallow work and can affect your professional career growth if your works is cognitive demanding.
    - Using social media extensively can harm the brain. It has actual cognitive consequences of being pervasive background form of anxiety.
- Life without social media :
    - It can be tough for a weak or two just after quitting the social media.
    - You can be more productive at your work.
    - It can preserve your ability to concentrate for longer period of time.