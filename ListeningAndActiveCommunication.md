## Listening and Active Communication

### 1. Active Listening

**Question 1 : What are the steps/strategies to do Active Listening?**

* Do not get distracted by your thoughts. 
* Focus on what the speaker is saying. 
* Try to show the speaker that you're listening using your subtle body language. 
* Try to node your head after the end of every sentence. 
* Repeat what the speaker has said just to be on the same page and to avoid misunderstanding. 
* If possible try to note down the important points of the conversation. 

### 2. Reflective Listening

**Question 2: According to Fisher's model, what are the key points of Reflective Listening?**

* This helps to open up the person you're talking to. 
* Due to rephrasing the points of the speaker, There is no room for misconception. 
* It increases the sense of understanding of the listener. 
* It can help to clarify the thoughts of the listener. 
  
### 3. Reflection

**Question 3 : What are the obstacles in your listening process?**

* While listening to others, I get lost in my thoughts.
* Without listening to the whole point, I try to jump to the conclusions. 


**Question 4 : What can you do to improve your listening?**

* I will try to be in the present while listening to someone. 
* I will listen to the whole point before analyzing the whole point. 

### 4. Types of Communication

**Question 5 : When do you switch to a Passive communication style in your day-to-day life?**

* When friends make fun of me. 
* When the bus conductor doesn't give me back my 5 rupee change. 
* When I am eating not-so-well cooked food and I appreciate the food with my words. 
  
**Question 6 : When do you switch into Aggressive communication styles in your day to day life?**

* When I am feeling sleepy and somebody tells me to do something. 

**Question 7 : When do you switch into Passive Aggressive (sarcasm/gossiping/ taunts/ silent treatment and others) communication styles in your day to day life?**

* When I am at a party talking to my friends about the management of the party. 

**Question 8 : How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?**

* I should talk about my preferences with my friends and relatives. 
* I should learn to say "no" when It is needed to be said. 
* I should learn about my feelings.
  



