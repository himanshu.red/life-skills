# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### Your takeaways from the video.

- If we make the new habits tiny, It is easier to repeat and one can be consistent with the behaviour for long term.
- If I can learn how to adopt tiny habits, this is life changing.
- The three factors of behaviour change: motivation, ability and  trigger.
- Doing tiny habits is easy to do and requires less motivation. That’s why it get’s easy to adopt a habit this way.
- The format of tiny habit is I have to interlock a tiny habit with an existing habit. Once you finish an existing habit, do a new tiny habit.

## 2. Tiny Habits by BJ Fogg - Core Message

### Your takeaways from the video in as much detail as possible

- The formula for behaviour is Motivation, Ability and Prompt.
- Easy to Do things require low motivation and vice-versa.
- Adopting a habit is easy but being consistent with it hard.
- The solution is to shrink the habit to it’s tiniest form. This way it becomes easy to adopt and make it permanent.
- A habit can be shrunk to it’s tiniest form by either reducing the quantity or doing the first step.
- There are three prompt which trigger the habit.
    - External prompt (Phone notification)
    - Internal prompt (thoughts and emotions)
    - Action prompt (chaining of a tiny behaviour with an existing behaviour)
- The action prompt is the best way to adopt a new habit. This prompt leverages the momentum we already have to a new small thing.
- Learning to celebrate after accomplishment of a work is the most critical component of habit development. It keeps the motivation up for doing the habit next time.
- When we accomplish something. Our confidence and motivation level ups. This is the success moment which gets created by the frequency of successes but not on the size of the success.

### How can you use B = MAP to make making new habits easier?

- Just after waking up, I’ll make my bed.
- After brushing my teeth, I’ll do five push-ups.
- After having my dinner, I’ll go for a small walk.
- After hitting the bed, I’ll think of one good thing that happened that day.

### Why it is important to "Shine" or Celebrate after each successful completion of habit?

- The celebration is important after completing a habit because It increases the confidence and motivation level and which works a drive to push myself further and make the habit a little bigger.

## 3. **1% Better Every Day Video**

### Your takeaways from the video (Minimum 5 points)

- The chances of doing an activity increases if I specify the time and place for doing that activity.
- Many people thing they lack motivation when what they really lack is clarity.
- Environments shape the desire. We should shape the environment according to our desires.
- Quantity works more better than quality in terms of an activity.
- The 2 minute rule
    - If it takes less than 2 minutes to do a work. Just do it right away.
    - It work for adopting a habit as well.
    - Break an activity in multiple sub-activities.
    - These sub-activities take less than 2 minutes so just keep doing those sub-activities back to back. Eventually you’ll end doing the whole activity.
- The Seinfield Theory talks about not breaking the chain of a habit. If you’ve adopted a habit, do it every single day.

## 4. Book Summary of Atomic Habits

### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

- Our approach to build a habit is opposite. We try to adopt a habit and then wish that someday after doing this habit for long enough. It will become our identity.
- Rather we should think of the identity first. For example, If I want to become healthy and I should think of myself a fit person which is bound to eat healthy and not consume frequent junk foods and works out regularly.

### Write about the book's perspective on how to make a good habit easier?

- Make It Obvious.
- Make it Attractive
- Make it Easy.
- Make it Satisfying.

### Write about the book's perspective on making a bad habit more difficult?

- Make it Invisible.
- Make it  Ugly.
- Make it Hard.
- Make it unsatisfying.

### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I will adopt the habit of eating healthy only from now on.
- I will create my identity as a healthy person. I will not try to eat junks everyday. May be once or twice a month.

### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- I will eliminate the habit of using phone after going to bed.
- I will throw the phone in my bag and go to sleep. I would try to read a couple pages on my Kindle.
- I would convince myself by reading some articles on the demerits of using phone in bed.