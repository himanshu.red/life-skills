# Prevention Of Sexual Harassment

## **what Kind of behavior causes sexual Harassment?**

* Commenting about colleague's clothing. 
* Body shaming. 
* Jokes based on sex or gender. 
* Deliberately asking the person out even after He/She is not interested. 
* Asking for sexual favors. 
* Spreading rumors about a colleague's personal or sexual life.
* Using foul language in the workplace.  
* Using obscene posters & screensavers in the workplace. 
* Sending emails & texts of a sexual nature. 
* Sexual Assault.
* Blocking movement.
* Inappropriate touching like kissing, hugging, patting, or rubbing forcefully.  
* Sexual gesturing.

## **What would you do in case you face or witness any incident or repeated incidents of such behavior?**

* If It happens in my workplace. 
  * I'll immediately tell my friends and colleagues about this incident. 
  * I'll report to my superiors. 
* If It happens in a public place. 
  * I will run from the place shouting recklessly.
  * I will immediately call the police and friends/relatives living nearby after finding a safe place.  