# Grit and Growth Mindset

## Paraphrase the video(Grit: the power of passion and perseverance | Angela Lee Duckworth) in a few lines. Use your own words.

- Grit is the one common characteristic among people who will succeed in their life.
- Grit is the long term consistency and hard work behind a single goal.
- Those people achieve their goal who keep on working on their goal with perseverance and consistency.

## What are your key takeaways from the video to take action on?

- IQ, Smartness and personality are not the main factor for achieving goals.
- One needs to have trust on himself and be honest to himself about the efforts he is putting for the better future that all the effort and sweat will be worth it at the end.

## Paraphrase (summarize) the video (Growth Mindset Introduction: What it is, How it Works, and Why it Matters) in a few lines in your own words.

- There are two kinds of people according to their mindset.
    - Fixed mindset
    - Growth mindset
- People with fixed mindset have this mentality that their skills are limited. No matter what they do, they can not gain something new.
- They do handle the criticism well, they fear the challenges and  don’t want to take feedback.
- People with growth mindset are the people who do better in life. They can go to any extent to achieve their goal.
- They take a new challenges as an opportunity to grow and learn from their mistakes.
- No matter how good you’re at something, there is always a room for improvement.
- A person have a growth mindset who wants improve in his life, career, relationship or any aspect.

## What are your key takeaways from the video to take action on?

- In the IT career, One must need to have a growth mindset as this field is full of challenges and competition. Harder the challenges better the reward.
- In this career, one must need to be technically sound, have a great communication skill. Therefore It gets very necessary to have a growth mindset.

## What is the Internal Locus of Control? What is the key point in the video?

- Internal locus of control is the sense of responsibility for once failure and success. The person who have internal locus of control knows that no external factors can take control of their own behaviour.
- Key Points from the video
    - We should have a strong internal locus of control rather than having external locus of control.
    - A person with external locus of control keeps giving excuses about his undone work.
    - A person with internal locus of control take responsibility of his failure and tries hard to succeed no matter what the circumstances are.

## Paraphrase the video(How to develop a Growth Mindset) in a few lines in your own words.

- To develop a growth mindset following things one should do
    - Believe in your ability.
    - Question your assumptions
    - Develop and have your own life curriculum. These curriculum can inspire you when you feel lost.
    - Honour and embrace the struggle.

## What are your key takeaways from the video to take action on?

- I should believe in my abilities.
- I should not get discouraged by the struggles I am currently facing.
- I should have a certain set of set of people whom I can look up during my struggles.

## What are one or more points that you want to take action on from the manual? (Maximum 3)

-  I will understand the users very well. I will serve them and the society by writing rock solid excellent software.
- I am very good friends with Confusion, Discomfort and Errors.
    - Confusion tells me there is more understanding to be achieved.
    - Discomfort tells me I have to make an effort to understand. I understand the learning process is uncomfortable.
    - Errors tell me what is not working in my code and how to fix it.
- I am 100 percent responsible for my learning.